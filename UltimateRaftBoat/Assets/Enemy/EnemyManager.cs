﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public enum EnemyType
    { 
        Fish,
        Seabull,
        Shake_Small,
        Shake_Big,
        GhostPirate_Blade,
        GhostPirate_Gun,
    }

    public int m_HP;
    public float m_MoveSpeed;
    private bool m_CanBeAttack;
}
