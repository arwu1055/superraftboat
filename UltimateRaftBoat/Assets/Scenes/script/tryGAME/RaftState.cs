﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum buildState
{
    Raft,
    CanBuild,
    None
};


public class RaftState : MonoBehaviour
{
    public buildState m_buildState = buildState.None;
    
    public GameObject Glass;
    public GameObject Raft;

   // public GameObject goRaftGrp;

    public void SetState()
    {
        if (m_buildState == buildState.Raft)
        {
            Raft.SetActive(true);
            Glass.SetActive(false);
        }

        else if (m_buildState == buildState.CanBuild)
        {
            Raft.SetActive(false);
            Glass.SetActive(true);
        }

        else
        {
            Raft.SetActive(false);
            Glass.SetActive(false);
        }

    }

    public void SetState(buildState NewState)
    {
        m_buildState = NewState;
        SetState();
       
    }






}
