﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRaft : MonoBehaviour
{
    public GameObject MagicBOX;

    public GameObject RaftGrp;

    public RaftState[,] ArrRaft;



    void Start()
    {

        if(RaftGrp==null)
        {
            RaftGrp = this.gameObject;
        }



        ArrRaft = new RaftState[19,13];
        for (int i=0;i< ArrRaft.GetLength(0); i++)
        {
            for (int b = 0; b < ArrRaft.GetLength(1); b++)
            {
                GameObject RaftAAA = GameObject.Instantiate(MagicBOX);
                Vector3 pos = Vector3.zero;
                pos.x = i;
                pos.y = 0F;
                pos.z = b;
                RaftAAA.transform.position = pos;
               // RaftAAA.GetComponent<RaftState>().goRaftGrp = RaftGrp;
                RaftAAA.GetComponent<RaftState>().SetState();
                RaftAAA.transform.SetParent(RaftGrp.transform);
                ArrRaft[i,b] = RaftAAA.GetComponent<RaftState>();
               
            }
        }
       

        for (int i = (ArrRaft.GetLength(0)/2)-1; i <=(ArrRaft.GetLength(0)/2)+1; i++)
        {
            for (int b = (ArrRaft.GetLength(1) / 2)- 1; b <= (ArrRaft.GetLength(1) / 2)+1; b++)
            {
                ArrRaft[i, b].SetState(buildState.Raft);
                CheckNeibors();
            }
        }



    }


    void Update()
    {



        if (Input.GetMouseButton(0))
        {
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rh;
            if (Physics.Raycast(r, out rh, 1000.0f, 1 << LayerMask.NameToLayer("Terrain")))
            {
                int x = Mathf.RoundToInt(rh.point.x);  //四捨五入
                int z = Mathf.RoundToInt(rh.point.z);

                if (x < ArrRaft.GetLength(0)&& x>=0 && z < ArrRaft.GetLength(1)&&z>=0)
                {
                    if (ArrRaft[x, z].m_buildState == buildState.CanBuild)
                    {

                        ArrRaft[x, z].SetState(buildState.Raft);
                        CheckNeibors();
                    }
                }
             
            }


        }





        if (Input.GetMouseButton(1))
        {
            Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rh;
            if (Physics.Raycast(r, out rh, 1000.0f, 1 << LayerMask.NameToLayer("Terrain")))
            {
                int x = Mathf.RoundToInt(rh.point.x);  //四捨五入
                int z = Mathf.RoundToInt(rh.point.z);
                ArrRaft[x, z].SetState(buildState.None);
                CheckNeibors();
            }
        }

    }


    void CheckNeibors()
    {

        for(int x=0;x <ArrRaft.GetLength(0); x++)
        {
            for (int z = 0; z < ArrRaft.GetLength(1); z++)
            {

                if (ArrRaft[x , z].m_buildState == buildState.CanBuild)
                    ArrRaft[x , z].SetState(buildState.None);
            }
        }
        for (int x = 0; x < ArrRaft.GetLength(0); x++)
        {
            for (int z = 0; z < ArrRaft.GetLength(1); z++)
            {
                CheckNeibors(x, z);
            }
        }


    }

    void CheckNeibors(int x,int z)
    {

        if (ArrRaft[x, z].m_buildState == buildState.Raft)
        {

            if (x + 1 < ArrRaft.GetLength(0))
                if (ArrRaft[x + 1, z].m_buildState == buildState.None)
                    ArrRaft[x + 1, z].SetState(buildState.CanBuild);

            if (x - 1 >= 0)
                if (ArrRaft[x - 1, z].m_buildState == buildState.None)
                    ArrRaft[x - 1, z].SetState(buildState.CanBuild);

            if (z + 1 < ArrRaft.GetLength(1))
                if (ArrRaft[x, z + 1].m_buildState == buildState.None)
                    ArrRaft[x, z + 1].SetState(buildState.CanBuild);

            if (z - 1 >= 0)
                if (ArrRaft[x, z - 1].m_buildState == buildState.None)
                    ArrRaft[x, z - 1].SetState(buildState.CanBuild);
        }


    }





}
